CREATE TABLE emp (
  empid INTEGER PRIMARY KEY auto_increment,
  name VARCHAR(100),
  salary FLOAT,
  age INTEGER
);


INSERT INTO emp (name, salary,age) VALUES ('amrutej', 44000, 25);
INSERT INTO emp (name, salary,age) VALUES ('anil', 44000, 25);
INSERT INTO emp (name, salary,age) VALUES ('supriya', 44000, 25);
INSERT INTO emp (name, salary,age) VALUES ('ashtaputre', 44000, 25);
